<?php

/**
 * @file
 * Scald: Views is a Scald Atom Provider for Views.
 */


/**
 * Implements hook_scald_atom_providers().
 */
function scald_views_scald_atom_providers() {
  return array(
    'views' => 'Views display available on the website',
  );
  // This code will never be hit, but is necessary to mark the string
  // for translation on localize.d.o.
  t('Views display available on the website');
}

/**
 * Implements hook_scald_add_form().
 *
 * Compose the form to create atoms, get the View and its display.
 */
function scald_views_scald_add_form(&$form, &$form_state) {
  $options = views_get_views_as_options($views_only = FALSE, $filter = 'enabled', $exclude_view = NULL, $optgroup = TRUE, $sort = TRUE);
  $form['view_and_display'] = array(
    '#type' => 'select',
    '#title' => t('Views - display'),
    '#options' => $options,
  );
  $form['args'] = array(
    '#type' => 'textfield',
    '#title' => t('Views arguments'),
    '#description' => t('Use "/" to separate values'),
  );
}

/**
 * Implements hook_scald_add_form_fill().
 */
function scald_views_scald_add_form_fill(&$atom, $form, $form_state) {
  // Get views args
  $args = explode('/', $form_state['values']['args']);
  // Load the view and its display.
  $view_display = $form_state['values']['view_and_display'];
  $data = explode(':', $view_display);
  $view = views_get_view($data[0]);
  if ($view) {
    $view->preview($data[1], array());
    $view->set_arguments($args);
    $atom->base_id = $view_display;
    $atom->title = $view->get_title();
    $atom->data['args'] = $args;
  }
}

/**
 *  * Implements hook_scald_player().
 *   */
function scald_views_scald_player() {
  return array(
    'views' => array(
      'name' => 'Views "player"',
      'description' => 'It\'s not really a player, but it will display the View.', 
      'type' => array('views'),
    ),
  );
}

/**
 * Implements hook_scald_prerender().
 */
function scald_views_scald_prerender($atom, $context, $options, $mode) {
  if ($mode == 'atom') {
    if ($context != 'sdl_library_item') {
      $data = explode(':', $atom->base_id);
      $view = views_get_view($data[0]);
      if ($view) {
        $args = !empty($atom->data['args']) ? array_values($atom->data['args']) : array();
        $view->set_display($data[1]);
        $view->set_arguments($args);
        $view->pre_execute();
        $view->execute();
        $atom->rendered->player = $view->render();
      }
    }
  }
}

/**
 * Implements hook_scald_fetch().
 */
function scald_views_scald_fetch($atom, $type) {
  $langcode = field_language('scald_atom', $atom, 'scald_thumbnail');
  if(!empty($atom->scald_thumbnail[$langcode][0]['uri'])) {
    $file = $atom->scald_thumbnail[$langcode][0]['uri'];
    if (file_exists($file)) {
      $atom->file_source = $atom->thumbnail_source = $file;
    }
  }
  $atom->omit_legend = TRUE;
}
